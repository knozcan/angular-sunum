(function (module){
  module.controller("SubredditController", SubredditController);

  SubredditController.$inject = ["$route"];

  function SubredditController($route){
    var vm = this;

    vm.links = $route.current.locals.links && $route.current.locals.links.data && $route.current.locals.links.data.children || [];
    vm.subredditName = $route.current.params.subreddit || "javascript";
    vm.subredditType = $route.current.params.type || "new";

  }
})(angular.module("app.subreddit"));
