(function (module){
  module.directive("link", link);

  function link(){
      return {
        scope: {
          linkData: "=link",
        },
        templateUrl: "src/components/app/ui/link/link.template.html"
      }
  }
})(angular.module("app.ui"));
