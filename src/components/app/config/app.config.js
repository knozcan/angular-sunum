(function (angular){
  angular.module("app").config(config);

  config.$inject = ["$routeProvider"];
  function config($routeProvider){
    $routeProvider
      .when("/subreddit/:subreddit/:type", {
        templateUrl: "src/components/app/subreddit/subreddit.template.html",
        controller: "SubredditController",
        controllerAs: "scvm",
        resolve: {
          links: ["apiSubreddit", "$route", "$q", function (apiSubreddit, $route, $q){
            var subreddit = $route.current.params.subreddit;
            var type = $route.current.params.type;

            return new $q(function (resolve, reject){
                apiSubreddit.getLinks(subreddit, type)
                .then(function (response) {
                  resolve(response.data);
                }, function (err) {
                  reject(err);
                });
            });
          }]
        }
      })
      .otherwise({
        redirectTo: "/subreddit/cats/hot"
      });
  }
})(angular);
