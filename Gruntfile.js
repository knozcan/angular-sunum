module.exports = function(grunt) {

  grunt.initConfig({
    "clean": {
        prod: ["build/"]
    },
    "copy": {
      prod: {
        files: [{
          expand: true,
          cwd: "src",
          src: "index.html",
          dest: "build/"
        }]
      }
    },
    "bower_concat": {
      prod:{
        dest: {
          "js": "build/lib.js"
        }
      }
    },
    "angular-builder": {
      options: {
        mainModule: "app",
        externalModules: "ngRoute"
      },
      prod: {
        src: "src/components/**/*.js",
        dest: "build/src.js"
      }
    },
    "ngtemplates": {
      options: {
        module: "app"
      },
      prod:{
        src: "src/components/**/*.template.html",
        dest: "build/tmpl.js"
      }
    }
  });

  grunt.loadNpmTasks("grunt-angular-builder");
  grunt.loadNpmTasks("grunt-contrib-clean");
  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks("grunt-bower-concat");
  grunt.loadNpmTasks("grunt-angular-templates");

  grunt.registerTask("default", ["clean", "angular-builder", "bower_concat", "copy", "ngtemplates"]);
}
